# OpenBSD Manpage Search

A Firefox WebExtension that allows the user to search manual page into official OpenBSD manual page server.
(man.openbsd.org)

- https://addons.mozilla.org/addon/openbsd-manpage/
